class User:
    USERNAME = "standard_user"
    LOCKED_OUT = "locked_out_user"
    PROBLEM = "problem_user"
    PERFORMANCE_GLITCH = "performance_glitch_user"
    PASSWORD = "secret_sauce"
    EMPTY = ""


class Title:
    TITLE = "Swag Labs"


class Label:
    TITLE = "PRODUCTS"


class Error:
    LOCKED_USER = "Epic sadface: Sorry, this user has been locked out."
    BAD_CREDENTIALS = "Epic sadface: Username and password do not match any user in this service"
    EMPTY_USERNAME = "Epic sadface: Username is required"
    EMPTY_PASSWORD = "Epic sadface: Password is required"

class Params:
    BAD_DATA = ["Gdgksghejrhg;weg[eogenbe", 123, "*&^%$#@", False, "ЛОГИН", "  "]
