from pytest import fixture
from pages.application import Application

from common.constants import Params, User


@fixture(scope="session")
def app(request):
    url = request.config.getoption("--base-url")
    headless = request.config.getoption("--headless")
    app = Application(headless, url)
    yield app
    app.browser_close()


def pytest_addoption(parser):
    parser.addoption(
        "--base-url",
        action="store",
        default="https://www.saucedemo.com/",
        help="enter base_url",
    ),
    parser.addoption(
        "--username",
        action="store",
        default="standard_user",
        help="enter username",
    ),
    parser.addoption(
        "--password",
        action="store",
        default="secret_sauce",
        help="enter password",
    ),
    parser.addoption(
        "--headless",
        action="store",
        default=False,
        help="launching browser without gui",
    ),


@fixture(params=[User.USERNAME, User.PROBLEM, User.PERFORMANCE_GLITCH])
def valid_login(request):
    return request.param


@fixture(params=Params.BAD_DATA)
def invalid_data(request):
    return request.param
