from hamcrest import assert_that, equal_to

from common.constants import Label, User, Error


class TestAuth:
    def test_valid_auth(self, app, valid_login):
        """
        1. Open the page
        2. Enter username and password
        3. Click on Login button
        4. Check the PRODUCTS caption
        """
        app.open_main_page()
        username = valid_login
        password = User.PASSWORD
        app.authorization.auth(username=username, password=password)
        assert_that(
            app.store_page.get_header_label().text, equal_to(Label.TITLE)
        )

    def test_locked_out_user(self, app):
        """
        1. Open the page.
        2. Enter locked out username and password
        3. Click on Login button
        4. Check the error message
        """
        app.open_main_page()
        username = User.LOCKED_OUT
        password = User.PASSWORD
        app.authorization.auth(username=username, password=password)
        assert_that(
            app.authorization.error_allert().text, equal_to(Error.LOCKED_USER)
        )

    def test_bad_login(self, app, invalid_data):
        """
        1. Open the page.
        2. Enter wrong username and valid password
        3. Click on Login button
        4. Check the error message
        """
        app.open_main_page()
        username = invalid_data
        password = User.PASSWORD
        app.authorization.auth(username=username, password=password)
        assert_that(
            app.authorization.error_allert().text,
            equal_to(Error.BAD_CREDENTIALS),
        )

    def test_bad_password(self, app, valid_login, invalid_data):
        """
        1. Open the page
        2. Enter valid username and wrong password
        3. Click on Login button
        4. Check the error message
        """
        app.open_main_page()
        username = valid_login
        password = invalid_data
        app.authorization.auth(username=username, password=password)
        assert_that(
            app.authorization.error_allert().text,
            equal_to(Error.BAD_CREDENTIALS),
        )

    def test_empty_login(self, app):
        """
        1. Open the page
        2. Enter only password
        3. Click on Login button
        4. Check the error message
        """
        app.open_main_page()
        username = User.EMPTY
        password = User.PASSWORD
        app.authorization.auth(username=username, password=password)
        assert_that(
            app.authorization.error_allert().text,
            equal_to(Error.EMPTY_USERNAME),
        )

    def test_empty_password(self, app):
        """
        1. Open the page
        2. Enter only username
        3. Click on Login button
        4. Check the error message
        """
        app.open_main_page()
        username = User.USERNAME
        password = User.EMPTY
        app.authorization.auth(username=username, password=password)
        assert_that(
            app.authorization.error_allert().text,
            equal_to(Error.EMPTY_PASSWORD),
        )
