from selenium.webdriver.common.by import By


class StorePageLocators:
    PRODUCTS = (By.XPATH, '//*[@class="title"]')
    BURGER = (By.XPATH, '//*[@id="react-burger-menu-btn"]')
    LOGOUT = (By.XPATH, '//*[@id="logout_sidebar_link"]')
