```
  Используя представленный фреймворк https://github.com/asevgrafov/skelet
  разработать набор авто-тестов UI для ресурса https://www.saucedemo.com/,
  которые покроют один или несколько перечисленных кейсов:
- Авторизация разными пользователями
- Отображение списка товаров
- Добавление товара в корзину из списка
- Добавление товара в корзину из карточки товара
- Отображение карточки товара
- Отображение страницы корзины
- Удаление товара из корзины
Можете воспользоваться своими наработками (фреймворком).

```
# Requirements
 * Python 3.6 or high
 * Allure 2.7.0


# Getting started

#### 1.Download source

```bash
$ git clone git@gitlab.com:tofelz13/test-task.git
```
#### 2. Make venv

```bash
$ make venv
```

#### 3. Run tests
```bash
$ make test
```

#### 4. Make a report
```bash
$ make report
```
