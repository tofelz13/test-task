from locators.store_page import StorePageLocators
import logging

logger = logging.getLogger()


class StorePage:
    def __init__(self, app):
        self.app = app

    def get_header_label(self):
        return self.app.driver.find_element(*StorePageLocators.PRODUCTS)

    def click_burger(self):
        self.app.driver.find_element(*StorePageLocators.BURGER).click()

    def logout(self):
        return self.app.driver.find_element(*StorePageLocators.LOGOUT)
